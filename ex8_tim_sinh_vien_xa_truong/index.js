
function tim() {
    var ten1 = document.getElementById("ten1").value;
    var ten2 = document.getElementById("ten2").value;
    var ten3 = document.getElementById("ten3").value;
    var x1 = document.getElementById("x1").value * 1;
    var x2 = document.getElementById("x2").value * 1;
    var x3 = document.getElementById("x3").value * 1;
    var xTruong = document.getElementById("x4").value * 1;
    var y1 = document.getElementById("y1").value * 1;
    var y2 = document.getElementById("y2").value * 1;
    var y3 = document.getElementById("y3").value * 1;
    var yTruong = document.getElementById("y4").value * 1;

    // 
    var toaDoXSv1 = getToaDoX(x1, xTruong);
    var toaDoYSv1 = getToaDoY(y1, yTruong);
    var toaDoXSv2 = getToaDoX(x2, xTruong);
    var toaDoYSv2 = getToaDoY(y2, yTruong);
    var toaDoXSv3 = getToaDoX(x3, xTruong);
    var toaDoYSv3 = getToaDoY(y3, yTruong);
    //

    var toaDoTong1 = Math.sqrt(toaDoXSv1 + toaDoYSv1);
    var toaDoTong2 = Math.sqrt(toaDoXSv2 + toaDoYSv2);
    var toaDoTong3 = Math.sqrt(toaDoXSv3 + toaDoYSv3);
    var max;
    if (toaDoTong1 > toaDoTong2 && toaDoTong1 > toaDoTong3) {
        max = ten1;
    } if (toaDoTong2 > toaDoTong1 && toaDoTong2 > toaDoTong3) {
        max = ten2;
    } if (toaDoTong3 > toaDoTong1 && toaDoTong3 > toaDoTong2) {
        max = ten3;
    }

    document.getElementById("result").innerHTML = max;
}

function getToaDoX(x, xTruong) {
    return Math.pow((xTruong - x), 2);
}

function getToaDoY(y, yTruong) {
    return Math.pow((yTruong - y), 2);
}

