

function docSo() {
    var num = document.getElementById("number").value * 1;
    var donVi = num % 10;
    var chuc = Math.floor(num / 10) % 10;
    var tram = Math.floor(num / 100);

    switch (tram) {
        case 1: {
            tram = "Một trăm"
            break
        }
        case 2: {
            tram = "Hai trăm"
            break
        }
        case 3: {
            tram = "Ba trăm"
            break
        }
        case 4: {
            tram = "Bốn trăm"
            break
        }
        case 5: {
            tram = "Năm trăm"
            break
        }
        case 6: {
            tram = "Sáu trăm"
            break
        }
        case 7: {
            tram = "Bảy trăm"
            break
        }
        case 8: {
            tram = "Tám trăm"
            break
        }
        case 9: {
            tram = "Chín trăm"
            break
        }
    }
    if (chuc % 10 == 0 && donVi != 0) {
        chuc = "lẻ"
    }
    switch (chuc) {
        case 0: {
            chuc = ""
            break
        }
        case 1: {
            chuc = "mười"
            break
        }
        case 2: {
            chuc = "hai mươi"
            break
        }
        case 3: {
            chuc = "ba mươi"
            break
        }
        case 4: {
            chuc = "bốn mươi"
            break
        }
        case 5: {
            chuc = "năm mươi"
            break
        }
        case 6: {
            chuc = "sáu mươi"
            break
        }
        case 7: {
            chuc = "bảy mươi"
            break
        }
        case 8: {
            chuc = "tám mươi"
            break
        }
        case 9: {
            chuc = "chín mươi"
            break
        }
    }
    switch (donVi) {
        case 0: {
            donVi = ""
            break
        }
        case 1: {
            donVi = "một"
            break
        }
        case 2: {
            donVi = "hai"
            break
        }
        case 3: {
            donVi = "ba"
            break
        }
        case 4: {
            donVi = "bốn"
            break
        }
        case 5: {
            donVi = "lăm"
            break
        }
        case 6: {
            donVi = "sáu"
            break
        }
        case 7: {
            donVi = "bảy"
            break
        }
        case 8: {
            donVi = "tám"
            break
        }
        case 9: {
            donVi = "chín"
            break
        }
    }
    document.getElementById("result").innerHTML = `${tram} ${chuc} ${donVi}`
}