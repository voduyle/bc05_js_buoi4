function tinhNgay() {
    var thang = document.getElementById("thang").value * 1;
    var nam = document.getElementById("nam").value * 1;
    var ngay = "";
    switch (thang) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12: {
            ngay = "có 31 ngày"
            break
        }
        case 4: case 6: case 9: case 11: {
            ngay = "có 30 ngày"
            break
        }
        case 2: {
            if (nam % 4 == 0 && nam % 100 != 0 || nam % 400 == 0) {
                ngay = "có 29 ngày"
            } else {
                ngay = "có 28 ngày"
            }
        }
    }
    document.getElementById("result").innerHTML = `Tháng ${thang} năm ${nam} ${ngay}`
}