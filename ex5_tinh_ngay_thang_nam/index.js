function homQua() {
    var ngay = document.getElementById("ngay").value * 1;
    var thang = document.getElementById("thang").value * 1;
    var nam = document.getElementById("nam").value * 1;

    var lastDayOfMonth;
    switch (thang) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12: {
            lastDayOfMonth = 31;
            break;
        }
        case 4: case 6: case 9: case 11: {
            lastDayOfMonth = 30;
            break;
        }
        case 2: {
            if ((nam % 4 == 0 && nam % 100 != 0) || nam % 400 == 0) {
                lastDayOfMonth = 29;
                break;
            } else {
                lastDayOfMonth = 28;
                break;
            }
        }
    }
    if (ngay == 1) {
        ngay = lastDayOfMonth;
        if (thang !== 1) {
            thang--;
        } else {
            thang = 12;
            nam--;
        }
    } else {
        ngay--;
    }

    document.getElementById("result").innerHTML = `${ngay} - ${thang} - ${nam}`;
}

function ngayMai() {
    var ngay = document.getElementById("ngay").value * 1;
    var thang = document.getElementById("thang").value * 1;
    var nam = document.getElementById("nam").value * 1;

    var lastDayOfMonth;
    switch (thang) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12: {
            lastDayOfMonth = 31;
            break;
        }
        case 4: case 6: case 9: case 11: {
            lastDayOfMonth = 30;
            break;
        }
        case 2: {
            if ((nam % 4 == 0 && nam % 100 != 0) || nam % 400 == 0) {
                lastDayOfMonth = 29;
                break;
            } else {
                lastDayOfMonth = 28;
                break;
            }
        }
    }
    if (ngay == lastDayOfMonth) {
        ngay = 1;

        if (thang != 12) {
            thang++;
        } else {
            thang = 1;
            nam++;
        }
    }

    document.getElementById("result").innerHTML = `${ngay} - ${thang} - ${nam}`;


}

